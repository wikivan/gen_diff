package com.mycompany.app;

import de.tudarmstadt.ukp.wikipedia.revisionmachine.api.RevisionApi;
import de.tudarmstadt.ukp.wikipedia.revisionmachine.api.Revision;
import de.tudarmstadt.ukp.wikipedia.revisionmachine.api.RevisionAPIConfiguration;

public class Step4 {
    public static void main(String[] args) throws Exception {
        RevisionAPIConfiguration config = new RevisionAPIConfiguration();
        config.setHost("localhost");
        config.setDatabase("Wiki");
        config.setUser("nascarsayan");
        config.setPassword("1234");

        config.setCharacterSet("UTF-8");
        config.setBufferSize(20000);
        config.setMaxAllowedPacket(1024 * 1024);

        RevisionApi rev = new RevisionApi(config);
        int aid = 2307950;
        Revision r;
        r = rev.getRevision(2307950, 80);
        System.out.println(r.toString() + "\t" + r.getRevisionText());
        int m = rev.getNumberOfUniqueContributors(aid);

        System.out.println(m);
    }
}
