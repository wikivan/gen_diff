package com.mycompany.app;
import de.tudarmstadt.ukp.wikipedia.revisionmachine.index.IndexGenerator;

public class Step3
{
    public static void main( String[] args )
    {
        String root = "./data/";
        System.out.println( "Create the index from the diff database!" );
        String arr[] = { root + "indexGeneratorConfig.txt" }; // Path to the config created in Step 1
        IndexGenerator.main(arr);
    }
}
