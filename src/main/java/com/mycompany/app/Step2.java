package com.mycompany.app;
import de.tudarmstadt.ukp.wikipedia.revisionmachine.difftool.DiffTool;

public class Step2
{
    public static void main( String[] args )
    {
        String root =  "./data/";
        System.out.println( "Create the diff data required by the API in sql or csv format!" );
        String arr[] = { root + "config1.xml" }; // Path to the config created in Step 1
        DiffTool.main(arr);
    }
}
