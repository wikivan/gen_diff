# JWPL Revision API

This is a demo project demonstrating how to use the [JWPL Revision Machine API](https://dkpro.github.io/dkpro-jwpl/RevisionMachine/).

## Prerequisites

The following packages are required to be installed in your machine:

* java with jdk, jre
* maven
* a code editor providing the feature of running individual Java files, like
    - Visual Studio Code
        * Install [this extension](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack) for full JAVA and Maven support.
    - IntelliJ IDEA
* a mysql database with full access
* a wikipedia dump file

## Project Structure
The java files of our interest are in the folder [src/main/java/com/mycompany/app](src/main/java/com/mycompany/app).

## Creating Diff

The first step is to extract the data from the wikipedia dump file (which is in xml format) and dump it to mysql database.

[This link](https://dkpro.github.io/dkpro-jwpl/RevisionMachine/) contains information about this step. This repo is just a demo showing that.
 
### 1. Creating the config file

A config file needs to be created that contains the information about the parameters regarding how the diffs are to be made from the dump file.
You need to provide the path to the wikiDump file, here it's [wikiDump.xml](data/wikiDump.xml).
For that run [Step1.java](src/main/java/com/mycompany/app/Step1.java).
A file like [config1.xml](data/config1.xml) will be generated.

### 2. Creating the sql data dump from wiki dump

Create the [sql dump file](data/output_1.sql) from the wiki dump file by running [Step2.java](src/main/java/com/mycompany/app/Step2.java).
You need to make certain changes to the dump file.

- Change 1 : Add <br>
`Use <dbName>;`<br>
before the first line.
- Change 2 : Replace<br>
`TYPE = MyISAM` by `ENGINE = MyISAM`.

creating [the new sql file](data/output_1_revised.sql).

### 3. Dump the sql file into the database

`mysql -u <user> -p < data/output_1_revised.sql`.

### 4. Creating the index

Run [Step2.java](src/main/java/com/mycompany/app/Step3.java). You need to create a [indexGeneratorConfig](data/indexGeneratorConfig), and provide it as an argument to IndexGenerator class constructor.

## Using the RevisionMachine API

You are now ready to use the RevisionMachine API. A demo is shown in [Step4.java](src/main/java/com/mycompany/app/Step4.java). The functionalities of the API can be found [here](https://github.com/dkpro/dkpro-jwpl/blob/master/de.tudarmstadt.ukp.wikipedia.revisionmachine/src/main/java/de/tudarmstadt/ukp/wikipedia/revisionmachine/api/RevisionApi.java).